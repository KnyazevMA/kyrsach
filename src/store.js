import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'


Vue.use(Vuex);

const store = new Vuex.Store({
    plugins: [createPersistedState()],
    state: {
     user: null,
     cart:[],
   },
   mutations: {
       setUser: (state, user) => state.user = user,
       setCart: (state, tovar) => state.cart.push({tovar:tovar}),
       deleteCart: (state) => state.cart = [],
       deleteUser: (state) => state.user = null,
       deletetovar: (state, index) => {
           state.cart = state.cart.filter(el => {
            return el.tovar[0]._id !== index;
        });
       }
    },
   getters: {
       getUser: (state) =>
       {
           return state.user
       },
       getCart: (state) => 
       { 
           return state.cart
       },
       LengthCart: (state) => 
       {
        return state.cart.length
       },
   }
 })

 export default store;