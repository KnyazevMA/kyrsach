import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './components/App.vue'
import ProductInfo from './components/ProductInfo.vue'
import ProductsCategoru from './components/ProductsCategoru.vue'
import login from './components/login.vue'
import checkout from './components/checkout.vue'
import register from './components/register.vue'
import single from './components/single.vue'
import store from './store.js'
import Vuex from 'vuex'
import zakaz from './components/zakaz.vue'


Vue.use(Vuex);

const routes = [
    { path: '/', component: ProductsCategoru },
    { path: '/product/:categoryid', component: ProductInfo },
    { path: '/login', component: login},
    { path: '/checkout', component: checkout, meta:{requiresAuth: true} },
    { path: '/register', component: register},
    { path: '/single/:id', component: single, meta:{requiresAuth: true} },
    { path: '/zakaz', component: zakaz},
]

const router = new VueRouter({
    routes
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)){
        if(store.getters.getUser === null){
            next({
                path: '/login',
                query: {redirect: to.fullPath}
            })
        } else {
            next()
        }
    } else {
        next()
    }
})

Vue.use(VueRouter);



new Vue({
    render: h => h(App),
    el: '#app',
    router,
    store,
})
