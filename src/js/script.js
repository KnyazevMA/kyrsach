var categores = [
    {
        "name": "Штани",
        "src": "imagesp/Штаны.jpg",
        "id": 1
    },
    {
        "name": "Футболки",
        "src": "imagesp/Футболки.jpg",
        "id": 2
    },
    {
        "name": "Рюкзаки",
        "src": "imagesp/Рюкзаки.jpg",
        "id": 3
    },
    {
        "name": "Куртки",
        "src": "imagesp/Куртки.jpg",
        "id": 4
    },
    {
        "name": "Взуття",
        "src": "imagesp/Кроссовки.jpg",
        "id": 5
    },
    {
        "name": "Світшоти",
        "src": "imagesp/Свитшоты.jpg",
        "id": 6
    }
]
var tovar = [
    {
        "name": "Cargo Black",
        "src": "imagesp/Cargo.jpg",
        "price": "590 грн",
        "id": 1
    },
    {
        "name": "Sector Blue",
        "src": "imagesp/Sector.jpg",
        "price": "690 грн",
        "id": 1
    },
    {
        "name": "Levis Gray",
        "src": "imagesp/Футболка1.jpg",
        "price": "390 грн",
        "id": 2
    },
    {
        "name": "Black Mychemol Romate",
        "src": "imagesp/Футболка2.jpg",
        "price": "870 грн",
        "id": 2
    },
    {
        "name": "Adidas Blue/Black",
        "src": "imagesp/Рюкзаки1.jpg",
        "price": "590 грн",
        "id": 3
    },
    {
        "name": "Vans Blue",
        "src": "imagesp/Рюкзаки2.jpg",
        "price": "250 грн",
        "id": 3
    },
    {
        "name": "Nike Blue",
        "src": "imagesp/Куртканайксиняя.jpg",
        "price": "750 грн",
        "id": 4
    },
    {
        "name": "Nike Black Parka",
        "src": "imagesp/Куртканайксиняя.jpg",
        "price": "999 грн",
        "id": 4
    },
    {
        "name": "Nike Black/White",
        "src": "imagesp/Кросы1.jpg",
        "price": "980 грн",
        "id": 5
    },
    {
        "name": "Asics White",
        "src": "imagesp/Кросы2.jpg",
        "price": "1500 грн",
        "id": 5
    },
    {
        "name": "Adidas Gray",
        "src": "imagesp/Свитшот1.jpg",
        "price": "550 грн",
        "id": 6
    },
    {
        "name": "Pobedov Blue",
        "src": "imagesp/Свитшот2.jpg",
        "price": "780 грн",
        "id": 6
    }
]
const ProductsList = {
    template: `
<div>
    <div class="header">
    <div class="header-top">
        <div class="logo">
            <a href="index.html"><img src="images/logo.png" alt=""></a>
        </div>
        <div class="container">
            <div class="head-top">

                <div class=" h_menu4">
                    <ul class="memenu skyblue">

                        <li class="grid"><a class="color2" href="#">Категорії</a>
                            <div class="mepanel">
                                <div class="row">
                                    <div class="col1">
                                        <div class="h_nav">
                                            <ul>
                                                <li><a href="products.html">Штани</a></li>
                                                <li><a href="products.html">Футболки</a></li>
                                                <li><a href="products.html">Рюкзаки</a></li>
                                                <li><a href="products.html">Куртки</a></li>
                                                <li><a href="products.html">Взуття</a></li>
                                                <li><a href="products.html">Світшоти</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>

                        <li><a class="color3" href="login.html">Авторизація</a></li>

                        <li><a class="color4" href="register.html">Реєстрація</a></li>

                        <li><a class="color6" href="contact77.html">Контакти</a></li>
                        <li>
                            <div class="cart box_1">
                                <a href="checkout.html">
                                    <h3>
                                        <div class="total">
                                            <span class="simpleCart_total"></span> (<span id="simpleCart_quantity"
                                                class="simpleCart_quantity"></span> товарів)</div>
                                        <img src="images/cart.png" alt="" />
                                    </h3>
                                </a>
                                <p><a href="javascript:;" class="simpleCart_empty">Очистити корзину</a></p>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="clearfix"> </div>
        </div>
    </div>
</div>
<div class="content">
    <div class="container">
        <div class="content-top">
            <h1>НОВА КОЛЕКЦІЯ</h1>
            <div class="products">
                <div class="categore" v-for="(item, index) in items">
                    <div class="categore-image">
                        <img v-bind:src="item.src">
                    </div>
                    <div>
                        <h4 class="product-title">
                            <router-link v-bind:to="'/product/' + item.id">
                                {{ item.name }}
                            </router-link>
                        </h4>
                    </div>
                </div>
            </div>
            <div class="content-bottom">
                <ul>
                    <li><a href="#"><img class="img-responsive" src="images/lo.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo1.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo2.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo3.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo4.png" alt=""></a></li>
                    <li><a href="#"><img class="img-responsive" src="images/lo5.png" alt=""></a></li>
                    <div class="clearfix"> </div>
                </ul>
            </div>
        </div>
        <div class="footer">
            <div class="container">
                <div class="footer-top-at">

                    <div class="col-md-4 amet-sed">
                        <h4>Графік роботи:</h4>
                        <ul class="nav-bottom">
                            <li><a href="#">Пн-Пт: 9:00-18:00</a></li>
                            <li><a href="#">Сб: 9:00-16:00</a></li>
                            <li><a href="#">Нд: выхідний</a></li>

                        </ul>
                    </div>
                    <div class="col-md-4 amet-sed ">
                        <h4>Контакти:</h4>
                        <p>Number: +380500148358</p>
                        <p>Viber: +380661234563</p>
                        <p>Email:sportmax@ukr.net</p>
                    </div>

                    <div class="col-md-4 amet-sed ">
                        <h4>Адреса:</h4>
                        <p>м. Запоріжжя</p>
                        <p>пр. Соборний 198</p>
                        <p>ООО: "SportMax"</p>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <div class="footer-class">
                <p>© 2015 New store All Rights Reserved | Design by <a href="http://w3layouts.com/" target="_blank">W3layouts</a>
                </p>
            </div>
        </div>
    </div>
</div>
</div>
`,

    data: function () {
        return {
            items: [],
            search: '',
            searchResult: [],

        };
    },
    mounted: function () {
        this.items = categores;
    },

    //     computed: {
    //         filteredItems: function() {
    //             console.log(this);
    //             if (!this.search) {
    //                 return this.items;
    //             }
    //             return this.items.filter(element => {
    //                 return element.item.toUpperCase().includes(this.search.toUpperCase());
    //             });
    //         },
    //     }
    // }
}
const ProductInfo = {
    template: `
        <div>
            <h2 v-if="product">
                This is page for product #{{ product.id }}: {{ product.src }} <br/> {{ product.price }}
            </h2>
            <h2 v-else>
                Product not found
            </h2>
        </div>
   `,
    data: function () {
        return {
            product: null,
        };
    },
    mounted: function () {
        this.product = tovar.find((item) => {
            return (item.id === Number(this.$route.params.id));
        });
    }
}

const routes = [
    { path: '/', component: ProductsList },
    { path: '/product/:id', component: ProductInfo }
]

const router = new VueRouter({
    routes
});
new Vue({
    el: '#app',
    router,
/*    data: {
        categores: [],
        tovar: [],
        search: '',
        searchResult: []
    },
    mounted: function(){
        this.categores = categores;
        this.tovar = tovar;
    },
    computed: {
        filteredItems: function() {
            if (!this.search) {
                return this.tovar;
            }
            return this.tovar.filter(element => {
                return element.name.toUpperCase().includes(this.search.toUpperCase());
            });
        },
    }
*/});