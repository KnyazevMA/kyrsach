'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var LoginSchema = new Schema({
  name: {
    type: String,
  },
  suname: {
    type: String,
  },
  batko: {
    type: String,
  },
  email: {
    type: String,
  },
  password: {
    type: String,
  },
  topassword: {
      type: String,
  },
  misto: {
    type: String,
  },
  number: {
    type: String,
  },
});

module.exports = mongoose.model('Logins', LoginSchema);
