'use strict';

var mongoose = require('mongoose'),
Login = mongoose.model('Logins');

exports.list_all_Login = function(req, res) {
    Login.find({}, function(err, Login) {
    if (err)
      res.send(err);
    res.json(Login);
  });
};

exports.create_a_Login = function(req, res) {
  var new_Login = new Login(req.body);
  new_Login.save(function(err, Login) {
    if (err)
      res.send(err);
    res.json(Login);
  });
};

exports.read_a_Login = function(req, res) {
    Login.findById(req.params.LoginID, function(err, Login) {
    if (err)
      res.send(err);
    res.json(Login);
  });
};

exports.update_a_Login = function(req, res) {
    Login.findOneAndUpdate({_id: req.params.LoginID}, req.body, {new: true}, function(err, Login) {
    if (err)
      res.send(err);
    res.json(Login);
  });
};

exports.delete_a_Login = function(req, res) {
    Login.remove({
    _id: req.params.LoginID
  }, function(err, Login) {
    if (err)
      res.send(err);
    res.json({ message: 'Login successfully deleted' });
  });
};

exports.read_login_by_auth = function(req, res) {
  Login.findOne({email : req.params.email, password : req.params.password}, function(err, Login) {
    if (err) {
      return res.send(err);
    } else {
      return res.json(Login);
    }
  });
};

exports.read_email = function(req, res) {
  Login.findOne({email : req.params.email}, function(err, Login) {
    if (err) {
      return res.send(err);
    } else {
      return res.json(Login);
    }
  });
};