'use strict';


var mongoose = require('mongoose'),
Product = mongoose.model('Products');

exports.list_all_productks = function(req, res) {
    Product.find({}, function(err, Product) {
    if (err)
      res.send(err);
    res.json(Product);
  });
};

exports.create_a_Product = function(req, res) {
  var new_Product = new Product(req.body);
  new_Product.save(function(err, Product) {
    if (err)
      res.send(err);
    res.json(Product);
  });
};

exports.read_a_Product = function(req, res) {
  Task.findById(req.params._id, function(err, Product) {
    if (err)
      res.send(err);
    res.json(Product);
  });
};

exports.update_a_Product = function(req, res) {
  Task.findOneAndUpdate({_id: req.params.ProductID}, req.body, {new: true}, function(err, Product) {
    if (err)
      res.send(err);
    res.json(Product);
  });
};

exports.delete_a_Product = function(req, res) {
    Product.remove({
    _id: req.params.ProductID
  }, function(err, Product) {
    if (err)
      res.send(err);
    res.json({ message: 'Product successfully deleted' });
  });
};
