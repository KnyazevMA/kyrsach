'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/todoListController');
  var product = require('../controllers/ControlerProduct');
  var login = require('../controllers/controllogin')  

  // todoList Routes
  app.route('/tasks')
    .get(todoList.list_all_tasks)
    .post(todoList.create_a_task);


  app.route('/tasks/:taskId')
    .get(todoList.read_a_task)
    .put(todoList.update_a_task)
    .delete(todoList.delete_a_task);

    app.route('/Products')
    .get(product.list_all_productks)
    .post(product.create_a_Product);


  app.route('/Products/:ProductId')
    .get(product.read_a_Product)
    .put(product.update_a_Product)
    .delete(product.delete_a_Product);

    app.route('/Login')
    .get(login.list_all_Login)
    .post(login.create_a_Login);


  app.route('/Login/:LoginId')
    .get(login.read_a_Login)
    .put(login.update_a_Login)
    .delete(login.delete_a_Login);

  app.route('/Login/emailandpass/:email&:password')
    .get(login.read_login_by_auth);

  app.route('/Login/email/:email')
  .get(login.read_email);
};

